package mpp;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import java.awt.Window.Type;
import java.awt.Toolkit;

/**
 * 
 * @author Sath
 * @version 0.1.1
 */
public class Debug {
	
	private JFrame		frmMppDebugConsole;
	private JTextArea	textArea;
	
	/**
	 * Create the application.
	 */
	public Debug() {
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		try{
			// Set System L&F
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}catch (UnsupportedLookAndFeelException e){}catch (ClassNotFoundException e){}catch (InstantiationException e){}catch (IllegalAccessException e){}
		frmMppDebugConsole = new JFrame();
		frmMppDebugConsole.setIconImage(Toolkit.getDefaultToolkit().getImage(Debug.class.getResource("/mpp/img/mpp_icon.png")));
		frmMppDebugConsole.setType(Type.UTILITY);
		frmMppDebugConsole.setTitle("MPP Debug Console");
		frmMppDebugConsole.setBounds(50, 50, 450, 300);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		frmMppDebugConsole.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		scrollPane.setViewportView(textArea);
	}
	
	public void println(String msg, boolean hideFromConsole) {
		System.out.println(msg);
		if(!hideFromConsole) textArea.setText(textArea.getText() + msg + "\n");
	}
	
	public void setVisible(boolean b) {
		frmMppDebugConsole.setVisible(b);
	}
	
	public boolean isVisible() {
		return frmMppDebugConsole.isVisible();
	}
}
