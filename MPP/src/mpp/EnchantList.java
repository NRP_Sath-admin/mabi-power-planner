package mpp;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import royalalchemi.util.FileSDs;
import net.miginfocom.swing.MigLayout;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.SplashScreen;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.hyperlink.AbstractHyperlinkAction;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.HyperlinkProvider;


/**
 * 
 * @author Sath
 * @version 0.1.1
 */
public class EnchantList extends JFrame {
	
	private static final long	serialVersionUID	= 1595618336915946829L;
	private JFrame				frmEnchantList;
	private ArrayList<String>	Enchants;
	private JPanel				contentPane;
	private JTextField			txfFilter;
	private JXTable				table;
	private static Debug		debug;
	private JComboBox<String>	cbxRank;
	private FileSDs				fileSDs;
	private SplashScreen		mySplash;
	
	/**
	 * Create the frame.
	 * 
	 * @param mySplash
	 */
	public EnchantList(Debug db, SplashScreen mySplash) {
		this.mySplash = mySplash;
		frmEnchantList = this;
		debug = db;
		debug.println("Checking for directories...", false);
		
		Enchants = new ArrayList<String>();
		setIconImage(Toolkit.getDefaultToolkit().getImage(EnchantList.class.getResource("/mpp/img/mpp_icon.png")));
		setTitle("Enchants");
		setBounds(100, 100, 822, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(frmEnchantList, "Important Note: Program assumes you meet all requirments, positive or negative.\n" + "Selected item in the main window will be enchanted (if allowed) with the selected\n" + "enchant when the \"Enchant\" button is pressed. It is also assumed (where applicable)\n" + "that max stats are gained when enchanted.\n\n" + "The search function looks at name, type, effects, what it can be enchanted onto and\n"
						+ "where it's found, it will not filter out ranks, use the drop menu above the search\n" + "box to control what rank is viewed.", "About", JOptionPane.PLAIN_MESSAGE);
			}
		});
		mnHelp.add(mntmAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[179.00][grow]", "[][grow][]"));
		DefaultTableModel dtm = new DefaultTableModel(new String[][] {}, new String[] { "Name", "Type, Rank", "Effect(s)", "Enchants Onto", "Comes From/On" }){
			private static final long	serialVersionUID	= 7375777567879930436L;
			boolean[]					columnEditables		= new boolean[] { false, false, false, false, false };
			
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		};
		dtm.addRow(new String[] { "Test1", "Test2", "Test3", "Test4", "Test5" });
		
		JLabel lblFiltersAndOptions = new JLabel("Filters and Options");
		lblFiltersAndOptions.setFont(new Font("Tahoma", Font.BOLD, 11));
		contentPane.add(lblFiltersAndOptions, "cell 0 0");
		
		JLabel lblList = new JLabel("List");
		lblList.setFont(new Font("Tahoma", Font.BOLD, 11));
		contentPane.add(lblList, "cell 1 0,alignx left");
		
		JPanel panel = new JPanel();
		contentPane.add(panel, "cell 0 1,grow");
		panel.setLayout(new MigLayout("", "[][grow]", "[][][][]"));
		
		JLabel lblRank = new JLabel("Rank");
		panel.add(lblRank, "cell 0 0,alignx left");
		
		cbxRank = new JComboBox<String>();
		cbxRank.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				displayList(txfFilter.getText());
			}
		});
		cbxRank.setModel(new DefaultComboBoxModel<String>(new String[] { "All", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" }));
		panel.add(cbxRank, "cell 1 0,growx");
		
		JLabel lblSearch = new JLabel("Search");
		panel.add(lblSearch, "cell 0 1,alignx trailing");
		
		txfFilter = new JTextField();
		txfFilter.addCaretListener(new CaretListener(){
			@Override public void caretUpdate(CaretEvent arg0) {
				displayList(txfFilter.getText());
			}
		});
		panel.add(txfFilter, "cell 1 1,growx");
		txfFilter.setColumns(10);
		
		JButton btnEnchant = new JButton("Enchant");
		panel.add(btnEnchant, "cell 1 2");
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				// TODO: Add code too wipe cache for enchant data and pull fresh
				// from the wiki.
			}
		});
		panel.add(btnUpdate, "cell 1 3");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scrollPane, "cell 1 1,grow");
		
		table = new JXTable();
		table.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Name", "Type, Rank", "Enchants Onto", "Effect(s)", "Comes On/From" }){
			private static final long				serialVersionUID	= 8573085068266177677L;
			@SuppressWarnings("rawtypes") Class[]	columnTypes			= new Class[] { String.class, String.class, String.class, String.class, Object.class };
			
			@SuppressWarnings({ "unchecked", "rawtypes" }) public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			
			boolean[]	columnEditables	= new boolean[] { false, false, false, false, false };
			
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(108);
		table.getColumnModel().getColumn(2).setPreferredWidth(136);
		table.getColumnModel().getColumn(3).setPreferredWidth(167);
		table.getColumnModel().getColumn(4).setPreferredWidth(161);
		table.setSurrendersFocusOnKeystroke(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFillsViewportHeight(true);
		table.setEditable(false);
		scrollPane.setViewportView(table);
		
		
		displayList(txfFilter.getText());
	}
	
	public void loadEnchantList() {
		debug.println("Loading enchants...", false);
		InputStream in = getClass().getResourceAsStream("enchant.list");
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		try{
			String n = "";
			while((n = br.readLine()) != null){
				if(!n.contains("--")){
					Enchants.add(n);
				}
			}
			br.close();
			in.close();
		}catch (IOException e1){
			e1.printStackTrace();
		}finally{
			getEnchantDataFromWiki();
		}
	}
	
	private void getEnchantDataFromWiki() {
		try{
			
			String OS = System.getProperty("os.name").toUpperCase();
			String loc = "";
			
			if(OS.contains("WIN")){
				loc = System.getenv("APPDATA") + "\\MPP\\Enchant_Data\\";
				
			}else if(OS.contains("MAC")){
				loc = System.getProperty("user.home") + "\\Library\\Application\\MPP\\Enchant_Data\\";
				
			}else if(OS.contains("NUX")){
				loc = System.getProperty("user.dir") + "\\MPP\\Enchant_Data\\";
			}
			
			fileSDs = new FileSDs(loc);
			
			String input = "SOL";
			String data;
			int n;
			int failed = 0;
			for(n = 0; n < Enchants.size(); n++){
				if(!(new File(loc + Enchants.get(n) + ".enh").exists())){
					debug.println("Enchant [" + Enchants.get(n) + "] not found, pulling data...", true);
					URL url = new URL("http://wiki.mabinogiworld.com/view/Template:DataEnchant" + Enchants.get(n));
					try{
						BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
						data = "";
						while((input = in.readLine()) != null){
							if(!input.isEmpty() && input.subSequence(0, 1).equals("|")){
								data += input;
							}
							if(input.contains("<noinclude>")){
								n = Integer.MAX_VALUE;
							}
						}
						fileSDs.serializeObject(data, Enchants.get(n), ".enh");
					}catch (FileNotFoundException fnfe){
						debug.println("Could not find address!\n" + fnfe.getLocalizedMessage(), false);
						failed++;
					}
				}else{
					debug.println("Enchant [" + Enchants.get(n) + "] found, skipping...", true);
				}
			}
			debug.println("Finished reading enchants! [" + (n - failed) + "/" + n + "]", false);
		}catch (MalformedURLException e){
			e.printStackTrace();
		}catch (IOException e){
			e.printStackTrace();
		}finally{
			if(mySplash != null){
				mySplash.close();
			}
		}
	}
	
	public void displayList(String filter) {
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Name");
		model.addColumn("Type, Rank");
		model.addColumn("Enchants Onto");
		model.addColumn("Effect(s)");
		model.addColumn("Comes on/From");
		for(int i = 0; i < Enchants.size(); i++){
			Vector<Object> temp = new Vector<Object>();
			try{
				String ench = (String) fileSDs.deserializeObject(new File(fileSDs.getCurrentDirectory() + Enchants.get(i) + ".enh"));
				if((Enchants.get(i).contains(filter) || getRawEnchantData("EnchOn",ench).contains(filter) || getRawEnchantData("ItemsDroppedBy",ench).contains(filter)) && (cbxRank.getSelectedItem().equals(getRawEnchantData("Rank",ench)) || cbxRank.getSelectedItem().equals("All"))){
					temp.add(getRawEnchantData("Name", ench));
					temp.add(getRawEnchantData("Rank", ench));
					temp.add("<html>" + getRawEnchantData("EnchOn", ench) + "</html>");
					temp.add("Lorem ipsum");
					temp.add("<html>" + getRawEnchantData("ItemsDroppedBy", ench) + "</html>");
					model.addRow(temp);
				}
			}catch (ClassNotFoundException | IOException e){
				temp.add("!--BadFile--! [" + Enchants.get(i) + ".enh]");
				debug.println("!--BadFile--! Deleting file [" + Enchants.get(i) + ".enh]",false);
				if(new File(fileSDs.getCurrentDirectory() + Enchants.get(i) + ".enh").delete()){
					debug.println("File deleted!", false);
				}else{
					debug.println("Error deleting file!", false);
				}
			}
		}
		table.setModel(model);
	}
	
	private String getRawEnchantData(String value, String enchantData) {
		try{
			int s = -1, e = -1;
			s = enchantData.indexOf("|" + value + "=");
			s += value.length() + 2;
			for(e = s; e < enchantData.length(); e++){
				if(enchantData.substring(e, e+1).equals("|")){
					return enchantData.substring(s, e).trim();
				}
			}
			enchantData.substring(s);
			if(s == -1){
				return "<--BadDataRequest-->";
			}else{
				return "";
			}
		}catch (StringIndexOutOfBoundsException e1){
			return "<--BadData-->\n" + e1.getMessage();
		}
	}
	
}
