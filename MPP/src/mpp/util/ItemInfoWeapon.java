package mpp.util;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class ItemInfoWeapon extends JPanel {
	private static final long	serialVersionUID	= 4739977760839591919L;
	private JLabel		lblDamage;
	private JLabel		lblInjury;
	private JLabel		lblCritical;
	private JLabel		lblDurability;
	private JLabel		lblBalance;
	private JLabel		lblUpgrades;
	private JLabel		lblPrefix;
	private JLabel		lblSuffix;
	private JTextPane	txpAdditonalInfo;
	
	public ItemInfoWeapon(Item item) {
		
		// this.setSize(250, 396);
		setLayout(new MigLayout("", "[grow]", "[][][][grow][][grow][][109.00,grow]"));
		
		JLabel lblName = new JLabel("<name>");
		add(lblName, "cell 0 0,alignx center");
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		add(panel, "cell 0 1,grow");
		panel.setLayout(new MigLayout("", "[][grow]", "[][][][][][]"));
		
		JLabel lblDamageText = new JLabel("Damage:");
		panel.add(lblDamageText, "cell 0 0");
		
		lblDamage = new JLabel("<min> ~ <max>");
		panel.add(lblDamage, "cell 1 0,alignx left");
		
		JLabel lblInjuryText = new JLabel("Injury:");
		panel.add(lblInjuryText, "cell 0 1");
		
		lblInjury = new JLabel("<min> ~ <max>");
		panel.add(lblInjury, "cell 1 1");
		
		JLabel lblCriticalText = new JLabel("Critical:");
		panel.add(lblCriticalText, "cell 0 2");
		
		lblCritical = new JLabel("<crit rate>%");
		panel.add(lblCritical, "cell 1 2");
		
		JLabel lblDurabilityText = new JLabel("Durability:");
		panel.add(lblDurabilityText, "cell 0 3");
		
		lblDurability = new JLabel("<max durability>");
		panel.add(lblDurability, "cell 1 3");
		
		JLabel lblBalanceText = new JLabel("Balance:");
		panel.add(lblBalanceText, "cell 0 4");
		
		lblBalance = new JLabel("<balance>%");
		panel.add(lblBalance, "cell 1 4");
		
		JLabel lblUpgradesText = new JLabel("Upgrades:");
		panel.add(lblUpgradesText, "cell 0 5");
		
		lblUpgrades = new JLabel("<max upgrades>");
		panel.add(lblUpgrades, "cell 1 5");
		
		JLabel lblEnchants = new JLabel("Enchants");
		add(lblEnchants, "cell 0 2,alignx center");
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		add(panel_1, "cell 0 3,grow");
		panel_1.setLayout(new MigLayout("", "[grow]", "[][][][]"));
		
		JLabel lblPrefixText = new JLabel("Prefix");
		panel_1.add(lblPrefixText, "cell 0 0,alignx center");
		
		lblPrefix = new JLabel("<prefix enchant data>");
		panel_1.add(lblPrefix, "cell 0 1");
		
		JLabel lblSuffixText = new JLabel("Suffix");
		panel_1.add(lblSuffixText, "cell 0 2,alignx center");
		
		lblSuffix = new JLabel("<suffix enchant data>");
		panel_1.add(lblSuffix, "cell 0 3");
		
		JLabel lblReforge = new JLabel("Reforge");
		add(lblReforge, "cell 0 4,alignx center");
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		add(panel_2, "cell 0 5,grow");
		panel_2.setLayout(new MigLayout("", "[]", "[grow][grow][grow]"));
		
		JLabel label = new JLabel("<reforge line 1>");
		panel_2.add(label, "cell 0 0");
		
		JLabel label_1 = new JLabel("<reforge line 2>");
		panel_2.add(label_1, "cell 0 1");
		
		JLabel label_2 = new JLabel("<reforge line 3>");
		panel_2.add(label_2, "cell 0 2");
		
		JLabel lblAdditionalInformation = new JLabel("Additional Information");
		add(lblAdditionalInformation, "cell 0 6,alignx center");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollPane, "cell 0 7,grow");
		
		txpAdditonalInfo = new JTextPane();
		scrollPane.setViewportView(txpAdditonalInfo);
		
		setItemData(item);
	}
	
	public void setItemData(Item item) {
		if(item != null){
			lblDamage.setText(item.getMinDamage() + " ~ " + item.getMaxDamage());
			lblInjury.setText(item.getMinInjury() + " ~ " + item.getMaxInjury());
			lblCritical.setText(item.getCritical() + "%");
			lblDurability.setText(item.getDurability() + "/" + item.getDurability());
			lblBalance.setText(item.getBalance() + "%");
		}
	}
	
}
