package mpp.util;

/**
 * stat# is the stat to be effected if it effects the character in a area such
 * as skill training it will only be displayed as text and not have any effect
 * on the item or character.
 * level# is the magnitude in which it effects the stat if applicable.
 * 
 * @author Sath
 * 
 */
public class Reforge {
	private String	stat1, stat2, stat3;
	private int		level1, level2, level3;
	
	public String getStat3() {
		return stat3;
	}
	
	public void setStat3(String stat3) {
		this.stat3 = stat3;
	}
	
	public String getStat2() {
		return stat2;
	}
	
	public void setStat2(String stat2) {
		this.stat2 = stat2;
	}
	
	public String getStat1() {
		return stat1;
	}
	
	public void setStat1(String stat1) {
		this.stat1 = stat1;
	}
	
	public int getLevel1() {
		return level1;
	}
	
	public void setLevel1(int level1) {
		this.level1 = level1;
	}
	
	public int getLevel2() {
		return level2;
	}
	
	public void setLevel2(int level2) {
		this.level2 = level2;
	}
	
	public int getLevel3() {
		return level3;
	}
	
	public void setLevel3(int level3) {
		this.level3 = level3;
	}
	
}
