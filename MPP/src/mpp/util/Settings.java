package mpp.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Settings implements Serializable {
	private static final long	serialVersionUID	= 8589293391809673223L;
	private Date				UPDATE;									// When
																			// to
																			// update
	private Date				UPDATED;									// When
																			// it
																			// was
																			// last
																			// updated
																			
	public Settings() {}
	
	public Date getUPDATE() {
		return UPDATE;
	}
	
	public void setUPDATE(Date UPDATE) {
		this.UPDATE = UPDATE;
	}
	
	public Date getUPDATED() {
		return UPDATED;
	}
	
	public void setUPDATED(Date uPDATED) {
		UPDATED = uPDATED;
	}
	
	public void schedual() {
		UPDATED = new Date();
		Calendar c = new GregorianCalendar();
		c.add(Calendar.DATE, 60);
		UPDATE = c.getTime();
	}
	
	public boolean isUpdateDay() {
		Date date = new Date();
		return (date.compareTo(UPDATE) >= 0);
	}
}
