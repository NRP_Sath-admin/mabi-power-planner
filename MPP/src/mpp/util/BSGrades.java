package mpp.util;

/**
 * Note: For some values in ranks A,A+,S+, and X
 * the values may be incorrect, not enough
 * information is available and a best guest was
 * made.
 * 
 * @author Sath
 * @version 0.1.1
 */
public enum BSGrades {
	D(new int[] { 1, 0, 0, 1, 2, 0, 0, 2, 0, 0, 0, 1, 0 }), Dp(new int[] { 1, 0, 1, 2, 3, 0, 1, 2, 0, 0, 0, 2, 0 }), C(new int[] { 1, 1, 1, 3, 4, 0, 1, 3, 0, 1, 0, 3, 0 }), Cp(new int[] { 1, 1, 2, 4, 4, 0, 1, 4, 0, 1, 0, 3, 0 }), B(new int[] { 2, 1, 2, 4, 4, 0, 2, 4, 0, 1, 0, 3, 0 }), Bp(new int[] { 2, 1, 3, 5, 5, 0, 2, 5, 0, 2, 1, 3, 1 }), A(new int[] { 2, 2, 3, 6, 5, 0, 2, 5, 0, 3, 1, 4, 1 }), Ap(new int[] { 2, 2, 4, 7, 5, 1, 2, 5, 1, 4, 1, 4, 1 }), S(new int[] { 2, 2, 4, 8, 5, 1, 2, 5, 1, 4,
			1, 4, 1 }), Sp(new int[] { 2, 2, 5, 9, 5, 1, 3, 5, 1, 5, 1, 4, 1 }), X(new int[] { 2, 2, 5, 10, 5, 1, 3, 5, 1, 5, 1, 4, 1 });
	
	int[]	value	= new int[13];
	
	BSGrades(int[] affect) {
		
	}
}
