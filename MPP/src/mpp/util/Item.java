package mpp.util;

import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import royalalchemi.util.CVal;
import royalalchemi.util.exceptions.ExisitingValueException;

/**
 * 
 * @author Sath
 * @version 0.1.1
 */
public class Item implements Serializable {
	private static final long	serialVersionUID	= 6954809003844803768L;
	
	private BSGrades			blacksmithGrade;
	private ImageIcon			image;
	private ArrayList<String>	enchantTypes;
	/**
	 * Slot types
	 * righthand
	 * lefthand
	 * twin_sword
	 */
	private ArrayList<String>	slots;
	private Reforge				reforge;
	private CVal				repairRates;
	private String				name;
	private boolean				enchant, reforgable, spUp, spirit;
	private String				prefixEnchant, suffixEnchant;
	private int					upgrades, gemUpgrades, durability;
	/**
	 * 0 = human
	 * 1 = human and elf
	 * 2 = elf
	 * 3 = elf and giant
	 * 4 = giant
	 * 5 = all
	 */
	private byte				race;
	private int					alchemyModifier;
	private String				element;
	private int					defense, protection;
	private int					minDamage, maxDamage, minInjury, maxInjury;
	private int					balance, critical;
	
	public Item() {
		slots = new ArrayList<String>();
		repairRates = new CVal();
		name = "";
		enchant = false;
		reforgable = false;
		spirit = true;
		spUp = false;
		upgrades = 0;
		gemUpgrades = 0;
	}
	
	public int getProtection() {
		return protection;
	}
	
	public void setProtection(int protection) {
		this.protection = protection;
	}
	
	public int getDefense() {
		return defense;
	}
	
	public void setDefense(int defense) {
		this.defense = defense;
	}
	
	public int getAlchemyModifier() {
		return alchemyModifier;
	}
	
	public void setAlchemyModifier(int alchemyModifier) {
		this.alchemyModifier = alchemyModifier;
	}
	
	public String getElement() {
		return element;
	}
	
	public void setElement(String element) {
		this.element = element;
	}
	
	public int getRepairCost(String rate) {
		return (int) repairRates.getValueOf(rate);
	}
	
	public void addRepairSet(String rate, int cost) {
		try{
			repairRates.addValue(rate, cost);
		}catch (ExisitingValueException e){
			e.printStackTrace();
		}
	}
	
	public ImageIcon getImage() {
		return image;
	}
	
	public void setImage(ImageIcon image) {
		this.image = image;
	}
	
	public String getEnchantType(int index) {
		return enchantTypes.get(index);
	}
	
	public void addEnchantTypes(String enchantType) {
		this.enchantTypes.add(enchantType);
	}
	
	public boolean isEnchant() {
		return enchant;
	}
	
	public void setEnchant(boolean enchant) {
		this.enchant = enchant;
	}
	
	public boolean isReforge() {
		return reforgable;
	}
	
	public void setReforge(boolean reforge) {
		this.reforgable = reforge;
	}
	
	public boolean isSpUp() {
		return spUp;
	}
	
	public void setSpUp(boolean spUp) {
		this.spUp = spUp;
	}
	
	public int getUpgrades() {
		return upgrades;
	}
	
	public void setUpgrades(int upgrades) {
		this.upgrades = upgrades;
	}
	
	public int getGemUpgrades() {
		return gemUpgrades;
	}
	
	public void setGemUpgrades(int gemUpgrades) {
		this.gemUpgrades = gemUpgrades;
	}
	
	public byte getRace() {
		return race;
	}
	
	public void setRace(byte race) {
		this.race = race;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSlots(int index) {
		return slots.get(index);
	}
	
	public void addSlots(String slot) {
		this.slots.add(slot);
	}
	
	public int getNumberOfSlotTypes() {
		return slots.size();
	}
	
	public String getPrefixEnchant() {
		return prefixEnchant;
	}
	
	public void setPrefixEnchant(String prefixEnchant) {
		this.prefixEnchant = prefixEnchant;
	}
	
	public String getSuffixEnchant() {
		return suffixEnchant;
	}
	
	public void setSuffixEnchant(String suffixEnchant) {
		this.suffixEnchant = suffixEnchant;
	}
	
	public boolean isSpirit() {
		return spirit;
	}
	
	public void setSpirit(boolean spirit) {
		this.spirit = spirit;
	}
	
	public int getMinDamage() {
		return this.minDamage;
	}
	
	public int getMaxDamage() {
		return this.maxDamage;
	}
	
	public int getMaxInjury() {
		return this.maxInjury;
	}
	
	public int getMinInjury() {
		return this.minInjury;
	}
	
	public void setMinDamage(int minDamage) {
		this.minDamage = minDamage;
	}
	
	public void setMaxDamage(int maxDamage) {
		this.maxDamage = maxDamage;
	}
	
	public void setMaxInjury(int maxInjury) {
		this.maxInjury = maxInjury;
	}
	
	public void setMinInjury(int minInjury) {
		this.minInjury = minInjury;
	}
	
	public Reforge getReforge() {
		return reforge;
	}
	
	public void setReforge(Reforge reforge) {
		this.reforge = reforge;
	}
	
	public int getCritical() {
		return this.critical;
	}
	
	public void setCritical(int critical) {
		this.critical = critical;
	}
	
	public int getDurability() {
		return durability;
	}
	
	public void setDurability(int durability) {
		this.durability = durability;
	}
	
	public int getBalance() {
		return balance;
	}
	
	public void setBalance(int balance) {
		this.balance = balance;
	}

	public BSGrades getBlacksmithGrade() {
		return blacksmithGrade;
	}

	public void setBlacksmithGrade(BSGrades blacksmithGrade) {
		this.blacksmithGrade = blacksmithGrade;
	}
}
