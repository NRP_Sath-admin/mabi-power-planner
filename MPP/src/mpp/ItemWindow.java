package mpp;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import royalalchemi.util.CVal;
import royalalchemi.util.FileSDs;
import royalalchemi.util.exceptions.ExisitingValueException;
import mpp.util.Item;
import net.miginfocom.swing.MigLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JList;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

import javax.swing.JButton;

import java.awt.Toolkit;
import java.awt.Window.Type;

import javax.swing.JTextField;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * 
 * @author Sath
 * @version 0.1.1
 */
public class ItemWindow {
	
	private JFrame				frmItemWindow;
	private static Debug		debug;
	private JLabel				lblItemImage;
	private JLabel				lblName;
	private JLabel				lblDamage;
	private JLabel				lblBalance;
	private JLabel				lblInjury;
	private JLabel				lblCritical;
	private JLabel				lblUpgrade;
	private JLabel				lblDurability;
	private JLabel				lblWaterAlchDamage;
	private JLabel				lblFireAlchDamage;
	private JLabel				lblClayAlchDamage;
	private JLabel				lblWindAlchDamage;
	private JComboBox<String>	cbxCatagory;
	private JList<String>		listItems;
	private CVal				items	= new CVal();
	private JLabel				lblDefense;
	private JLabel				lblProtection;
	private JTextField			txfFilter;
	private Main				main;
	private JLabel				lblImage;
	private Object				item;
	private JLabel				lblSpUp;
	private JLabel				lblSpirit;
	private JLabel				lblEnchant;
	private JLabel				lblReforge;
	private JLabel				lblCanCastFireball;
	private JLabel				lblCanCastThunder;
	private JLabel				lblCanCastIceSpear;
	private FileSDs				fileSDs;
	
	/**
	 * Create the application.
	 * 
	 * @param debug2
	 */
	public ItemWindow(Debug debug, Main main) {
		this.main = main;
		ItemWindow.debug = debug;
		ItemWindow.debug.println("Initializing GUI...", false);
		try{
			initialize();
		}catch (FileNotFoundException e){
			e.printStackTrace();
		}
	}
	
	private void clearData() {
		item = new Item();
		lblDefense.setText("");
		lblProtection.setText("");
		lblItemImage.setIcon(null);
		lblName.setText("");
		lblDamage.setText("");
		lblDurability.setText("");
		lblBalance.setText("");
		lblInjury.setText("");
		lblCritical.setText("");
		lblUpgrade.setText("");
		lblWaterAlchDamage.setText("0%");
		lblWaterAlchDamage.setForeground(Color.BLACK);
		
		lblFireAlchDamage.setText("0%");
		lblFireAlchDamage.setForeground(Color.BLACK);
		
		lblClayAlchDamage.setText("0%");
		lblClayAlchDamage.setForeground(Color.BLACK);
		
		lblWindAlchDamage.setText("0%");
		lblWindAlchDamage.setForeground(Color.BLACK);
		
		lblReforge.setText("\u2718");
		lblReforge.setForeground(Color.RED);
		
		lblEnchant.setText("\u2718");
		lblEnchant.setForeground(Color.RED);
		
		lblSpirit.setText("\u2714");
		lblSpirit.setForeground(Color.GREEN);
		
		lblSpUp.setText("\u2718");
		lblSpUp.setForeground(Color.RED);
		
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() throws FileNotFoundException {
		try{
			// Set System L&F
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}catch (UnsupportedLookAndFeelException e){}catch (ClassNotFoundException e){}catch (InstantiationException e){}catch (IllegalAccessException e){}
		
		String OS = System.getProperty("os.name").toUpperCase();
		String loc = "";
		
		if(OS.contains("WIN")){
			loc = System.getenv("APPDATA") + "\\MPP\\Items\\";
			
		}else if(OS.contains("MAC")){
			loc = System.getProperty("user.home") + "\\Library\\Application\\MPP\\Items\\";
			
		}else if(OS.contains("NUX")){
			loc = System.getProperty("user.dir") + "\\MPP\\Items\\";
		}
		
		fileSDs = new FileSDs(loc);
		
		item = new Item();
		frmItemWindow = new JFrame();
		frmItemWindow.setType(Type.POPUP);
		frmItemWindow.setIconImage(Toolkit.getDefaultToolkit().getImage(ItemWindow.class.getResource("/mpp/img/mpp_icon.png")));
		frmItemWindow.setTitle("Items");
		frmItemWindow.setBounds(650, 100, 501, 501);
		frmItemWindow.getContentPane().setLayout(new MigLayout("", "[][grow]", "[262px,grow][]"));
		JPanel panel = new JPanel();
		frmItemWindow.getContentPane().add(panel, "cell 0 0,growy");
		panel.setLayout(new MigLayout("", "[][98.00][][98.00]", "[75.00][][][][][][][][][][][][][][][][][]"));
		
		lblImage = new JLabel("Image");
		panel.add(lblImage, "cell 0 0");
		
		lblItemImage = new JLabel("");
		lblItemImage.setBackground(Color.BLACK);
		panel.add(lblItemImage, "cell 1 0");
		
		JLabel lblTextName = new JLabel("Name");
		panel.add(lblTextName, "cell 0 1");
		
		lblName = new JLabel("");
		panel.add(lblName, "cell 1 1");
		
		JLabel lblTextDamage = new JLabel("Damage");
		panel.add(lblTextDamage, "cell 0 2");
		
		lblDamage = new JLabel("");
		panel.add(lblDamage, "cell 1 2");
		
		JLabel lblTextDefense = new JLabel("Defense");
		panel.add(lblTextDefense, "cell 0 3");
		
		lblDefense = new JLabel("");
		panel.add(lblDefense, "cell 1 3");
		
		JLabel lblTextProtection = new JLabel("Protection");
		panel.add(lblTextProtection, "cell 0 4");
		
		lblProtection = new JLabel("");
		panel.add(lblProtection, "cell 1 4");
		
		JLabel lblTextDurability = new JLabel("Durability");
		panel.add(lblTextDurability, "cell 0 5");
		
		lblDurability = new JLabel("");
		panel.add(lblDurability, "cell 1 5");
		
		JLabel lblTextBalance = new JLabel("Balance");
		panel.add(lblTextBalance, "cell 0 6");
		
		lblBalance = new JLabel("");
		panel.add(lblBalance, "cell 1 6");
		
		JLabel lblTextInjury = new JLabel("Injury");
		panel.add(lblTextInjury, "cell 0 7");
		
		lblInjury = new JLabel("");
		panel.add(lblInjury, "cell 1 7");
		
		JLabel lblTextCritical = new JLabel("Critical");
		panel.add(lblTextCritical, "cell 0 8");
		
		lblCritical = new JLabel("");
		panel.add(lblCritical, "cell 1 8");
		
		JLabel lblTextUpgrade = new JLabel("Upgrade");
		lblTextUpgrade.setToolTipText("Upgrade/Gem Upgrade");
		panel.add(lblTextUpgrade, "cell 0 9");
		
		lblUpgrade = new JLabel("");
		panel.add(lblUpgrade, "cell 1 9");
		
		JLabel label = new JLabel("");
		label.setToolTipText("Water");
		label.setIcon(new ImageIcon(ItemWindow.class.getResource("/mpp/img/Water_Crystal.png")));
		panel.add(label, "cell 0 10,alignx center");
		
		lblWaterAlchDamage = new JLabel("0%");
		panel.add(lblWaterAlchDamage, "cell 1 10");
		
		JLabel label_4 = new JLabel("");
		label_4.setToolTipText("This item can't cast Fireball.");
		label_4.setIcon(new ImageIcon(ItemWindow.class.getResource("/mpp/img/20px-Fireball.png")));
		panel.add(label_4, "cell 2 10,alignx center");
		
		lblCanCastFireball = new JLabel("\u2718");
		lblCanCastFireball.setForeground(Color.RED);
		panel.add(lblCanCastFireball, "cell 3 10");
		
		JLabel label_1 = new JLabel("");
		label_1.setToolTipText("Fire");
		label_1.setIcon(new ImageIcon(ItemWindow.class.getResource("/mpp/img/Fire_Crystal.png")));
		panel.add(label_1, "cell 0 11,alignx center");
		
		lblFireAlchDamage = new JLabel("0%");
		panel.add(lblFireAlchDamage, "cell 1 11,alignx left,aligny bottom");
		
		JLabel label_5 = new JLabel("");
		label_5.setToolTipText("This item can't cast Thunder.");
		label_5.setIcon(new ImageIcon(ItemWindow.class.getResource("/mpp/img/20px-Thunder.png")));
		panel.add(label_5, "cell 2 11,alignx center");
		
		lblCanCastThunder = new JLabel("\u2718");
		lblCanCastThunder.setForeground(Color.RED);
		panel.add(lblCanCastThunder, "cell 3 11");
		
		JLabel label_2 = new JLabel("");
		label_2.setToolTipText("Clay");
		label_2.setIcon(new ImageIcon(ItemWindow.class.getResource("/mpp/img/Clay_Crystal.png")));
		panel.add(label_2, "cell 0 12,alignx center");
		
		lblClayAlchDamage = new JLabel("0%");
		panel.add(lblClayAlchDamage, "cell 1 12");
		
		JLabel label_6 = new JLabel("");
		label_6.setToolTipText("This item can't cast Ice Spear.");
		label_6.setIcon(new ImageIcon(ItemWindow.class.getResource("/mpp/img/20px-Ice_Spear.png")));
		panel.add(label_6, "cell 2 12,alignx center");
		
		lblCanCastIceSpear = new JLabel("\u2718");
		lblCanCastIceSpear.setForeground(Color.RED);
		panel.add(lblCanCastIceSpear, "cell 3 12");
		
		JLabel label_3 = new JLabel("");
		label_3.setToolTipText("Wind");
		label_3.setIcon(new ImageIcon(ItemWindow.class.getResource("/mpp/img/Wind_Crystal.png")));
		panel.add(label_3, "cell 0 13,alignx center");
		
		lblWindAlchDamage = new JLabel("0%");
		panel.add(lblWindAlchDamage, "cell 1 13");
		
		JLabel label_7 = new JLabel("");
		label_7.setToolTipText("This item can't cast Party Healing.");
		label_7.setIcon(new ImageIcon(ItemWindow.class.getResource("/mpp/img/20px-Party_Healing.png")));
		panel.add(label_7, "cell 2 13,alignx center");
		
		JLabel lblCanCastPartyHeal = new JLabel("\u2718");
		lblCanCastPartyHeal.setForeground(Color.RED);
		panel.add(lblCanCastPartyHeal, "cell 3 13");
		
		JLabel lblTextReforge = new JLabel("Reforge");
		panel.add(lblTextReforge, "cell 0 14");
		
		lblReforge = new JLabel("\u2718");
		lblReforge.setForeground(Color.RED);
		panel.add(lblReforge, "cell 1 14");
		
		JLabel lblTextEnchant = new JLabel("Enchant");
		panel.add(lblTextEnchant, "cell 0 15");
		
		lblEnchant = new JLabel("\u2718");
		lblEnchant.setForeground(Color.RED);
		panel.add(lblEnchant, "cell 1 15");
		
		JLabel lblTextSpirit = new JLabel("Spirit");
		panel.add(lblTextSpirit, "cell 0 16");
		
		lblSpirit = new JLabel("\u2714");
		lblSpirit.setForeground(Color.GREEN);
		panel.add(lblSpirit, "cell 1 16");
		
		JLabel lblTextSpecialUpgrade = new JLabel("Sp. Up.");
		panel.add(lblTextSpecialUpgrade, "cell 0 17");
		
		lblSpUp = new JLabel("\u2718");
		lblSpUp.setForeground(Color.RED);
		panel.add(lblSpUp, "cell 1 17");
		
		JPanel panel_1 = new JPanel();
		frmItemWindow.getContentPane().add(panel_1, "cell 1 0,grow");
		panel_1.setLayout(new MigLayout("", "[grow]", "[][][][][grow]"));
		
		JLabel lblCatagory = new JLabel("Catagory");
		panel_1.add(lblCatagory, "cell 0 0");
		
		cbxCatagory = new JComboBox<String>();
		cbxCatagory.addActionListener(new ActionListener(){
			@Override public void actionPerformed(ActionEvent arg0) {
				UpdateItemsList(txfFilter.getText());
			}
		});
		panel_1.add(cbxCatagory, "cell 0 1,growx");
		
		JLabel lblFilter = new JLabel("Filter");
		panel_1.add(lblFilter, "flowx,cell 0 2");
		
		JLabel lblList = new JLabel("List");
		panel_1.add(lblList, "cell 0 3");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panel_1.add(scrollPane, "cell 0 4,grow");
		
		listItems = new JList<String>();
		listItems.addMouseListener(new MouseAdapter(){
			@Override public void mouseClicked(MouseEvent arg0) {
				String name = listItems.getSelectedValue();
				System.out.println(name);
				if(name.contains("'")) name = name.replaceAll("'", "%27");
				
				if(name.contains(" ")) name.replaceAll(" ", "_");
				
				if(name != null || !name.isEmpty()) readWikiData(name);
			}
		});
		scrollPane.setViewportView(listItems);
		debug.println("GUI initialized.", false);
		
		debug.println("Loading Item.list...", false);
		InputStream in = getClass().getResourceAsStream("Item.list");
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		try{
			String n = "", currentCat = "";
			DefaultComboBoxModel<String> categories = new DefaultComboBoxModel<String>();
			while((n = br.readLine()) != null){
				if(n.contains("--") && !n.contains("//")){
					currentCat = n.substring(2);
					categories.addElement(n.substring(2));
				}else if(!n.contains("//")){
					try{
						items.addValue(n, currentCat);
					}catch (ExisitingValueException e){
						e.printStackTrace();
					}
				}
			}
			br.close();
			in.close();
			cbxCatagory.setModel(categories);
			
			txfFilter = new JTextField();
			txfFilter.addCaretListener(new CaretListener(){
				@Override public void caretUpdate(CaretEvent arg0) {
					UpdateItemsList(txfFilter.getText());
				}
			});
			txfFilter.setToolTipText("Case senstitive.");
			panel_1.add(txfFilter, "cell 0 2,growx");
			txfFilter.setColumns(10);
			
			JButton btnAddItem = new JButton("Add Item");
			btnAddItem.setToolTipText("Add item to character.");
			btnAddItem.addActionListener(new ActionListener(){
				@Override public void actionPerformed(ActionEvent arg0) {
					main.addItem(item);
				}
			});
			frmItemWindow.getContentPane().add(btnAddItem, "cell 0 1,alignx center");
			
			UpdateItemsList(txfFilter.getText());
		}catch (IOException e1){
			e1.printStackTrace();
		}
	}
	
	public boolean isVisible() {
		return frmItemWindow.isVisible();
	}
	
	private void readWikiData(String pageName) {
		clearData();
		String tempName = pageName;
		File tempFile = new File(fileSDs.getCurrentDirectory() + pageName + ".itd");
		System.out.println("Looking for file " + tempFile.getAbsolutePath());
		item = new Item();
		((Item) item).setSpirit(true);
		
		if(tempFile.exists()){
			debug.println("File found attempting to load!", false);
			try{
				item = fileSDs.deserializeObject(tempFile);
			}catch (ClassNotFoundException e){
				debug.println("!--BadFile--!", false);
			}catch (IOException e){
				e.printStackTrace();
			}
		}else{
			debug.println("File not found! Attempting to load from wiki...", false);
			try{
				URL url = new URL("http://wiki.mabinogiworld.com/view/Template:Data" + pageName);
				BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
				String input = "SOL";
				int minD = -1, maxD = -1;
				int minI = -1, maxI = -1;
				int ug = 0, gug = 0;
				while((input = in.readLine()) != null){
					if(!input.isEmpty() && input.subSequence(0, 1).equals("|")){
						if(input.contains("Category=")){
							String[] slots = input.substring(10).split("/");
							for(String string : slots){
								if(!string.isEmpty()){
									System.out.println("Adding slot: " + string);
									((Item)item).addSlots(string);
								}
							}
						}
						if(input.contains("Name=")){
							String temp = input.substring(6);
							((Item) item).setName(temp);
							lblName.setText(temp);
							debug.println("Found Name...", false);
							InputStream in2 = getClass().getResourceAsStream("SpUpList");
							BufferedReader br2 = new BufferedReader(new InputStreamReader(in2));
							String checkName = "";
							boolean canSpUp = false;
							while((checkName = br2.readLine()) != null){
								if(checkName.equals(temp)) canSpUp = true;
							}
							br2.close();
							in2.close();
							if(!canSpUp){
								debug.println("Not found in SpUp List...", false);
								((Item) item).setSpUp(false);
							}else{
								debug.println("Found in SpUp List...", false);
								((Item) item).setSpUp(true);
								lblSpUp.setText("\u2714");
								lblSpUp.setForeground(Color.GREEN);
							}
						}
						
						if(input.contains("MinDamage=")){
							minD = Integer.parseInt(input.substring(11));
							((Item) item).setMinDamage(minD);
							debug.println("Found Min Damage...", false);
						}
						if(input.contains("MaxDamage=")){
							maxD = Integer.parseInt(input.substring(11));
							((Item) item).setMaxDamage(maxD);
							debug.println("Found Max Damage...", false);
						}
						if(minD != -1 && maxD != -1){
							lblDamage.setText(minD + " ~ " + maxD);
						}
						
						if(input.contains("Balance=")){
							lblBalance.setText(input.substring(9) + "%");
							((Item) item).setBalance(Integer.parseInt(input.substring(9)));
							debug.println("Found Balance...", false);
						}
						
						if(input.contains("Durability=")){
							lblDurability.setText(Integer.toString(Integer.parseInt(input.substring(12)) / 1000));
							((Item) item).setDurability(Integer.parseInt(input.substring(12)) / 1000);
							debug.println("Found Durability...", false);
						}
						
						if(input.contains("MinInjury=")){
							minI = Integer.parseInt(input.substring(11));
							((Item) item).setMinInjury(minI);
							debug.println("Found Min Damage...", false);
						}
						if(input.contains("MaxInjury=")){
							maxI = Integer.parseInt(input.substring(11));
							((Item) item).setMaxInjury(maxI);
							debug.println("Found Max Damage...", false);
						}
						if(minI != -1 && maxI != -1){
							lblInjury.setText(minI + "% ~ " + maxI + "%");
						}
						if(input.contains("Critical=")){
							lblCritical.setText(Integer.parseInt(input.substring(10)) + "%");
							((Item) item).setCritical(Integer.parseInt(input.substring(10)));
							debug.println("Found Critical Rate...", false);
						}
						if(input.length() > 9){
							if(input.contains("Upgrades=") && !input.contains("Gem")){
								ug = Integer.parseInt(input.substring(10));
								((Item) item).setUpgrades(ug);
								debug.println("Found Upgrade Limit...", false);
							}
						}
						if(input.contains("GemUpgrades=")){
							gug = Integer.parseInt(input.substring(13));
							((Item) item).setGemUpgrades(gug);
							debug.println("Found Gem Upgrade Limit...", false);
						}
						lblUpgrade.setText(ug + "/" + gug);
						
						if(input.contains("Defense=")){
							lblDefense.setText(input.substring(9));
							((Item) item).setDefense(Integer.parseInt(input.substring(9)));
							debug.println("Found Defense...", false);
						}
						if(input.contains("Protection=")){
							lblDefense.setText(input.substring(12));
							((Item) item).setProtection(Integer.parseInt(input.substring(12)));
							debug.println("Found Protection...", false);
						}
						
						if(input.contains("AlchemyElementalBonus=")){
							if(input.contains("fire")){
								int start = 0, end = input.length();
								start = input.indexOf("AlchemyElementalBonus=&quot;") + 28;
								for(int n = start; n < end; n++){
									if(input.substring(n, n + 6).equals("&quot;")){
										end = n;
									}
								}
								int mod = Integer.parseInt(input.substring(start, end));
								
								lblFireAlchDamage.setForeground(Color.GREEN);
								lblFireAlchDamage.setText((mod * 3) + "%");
								
								lblWaterAlchDamage.setForeground(Color.RED);
								lblWaterAlchDamage.setText(mod + "%");
								
								lblClayAlchDamage.setForeground(Color.RED);
								lblClayAlchDamage.setText(mod + "%");
								
								lblWindAlchDamage.setForeground(Color.RED);
								lblWindAlchDamage.setText(mod + "%");
								
								((Item) item).setAlchemyModifier(mod);
								((Item) item).setElement("fire");
								
							}else if(input.contains("wind")){
								int start = 0, end = input.length();
								start = input.indexOf("AlchemyElementalBonus=&quot;") + 28;
								for(int n = start; n < end; n++){
									if(input.substring(n, n + 6).equals("&quot;")){
										end = n;
									}
								}
								int mod = Integer.parseInt(input.substring(start, end));
								
								lblFireAlchDamage.setForeground(Color.RED);
								lblFireAlchDamage.setText((mod) + "%");
								
								lblWaterAlchDamage.setForeground(Color.RED);
								lblWaterAlchDamage.setText(mod + "%");
								
								lblClayAlchDamage.setForeground(Color.RED);
								lblClayAlchDamage.setText(mod + "%");
								
								lblWindAlchDamage.setForeground(Color.GREEN);
								lblWindAlchDamage.setText(mod * 3 + "%");
								
								((Item) item).setAlchemyModifier(mod);
								((Item) item).setElement("wind");
								
							}else if(input.contains("water")){
								int start = 0, end = input.length();
								start = input.indexOf("AlchemyElementalBonus=&quot;") + 28;
								for(int n = start; n < end; n++){
									if(input.substring(n, n + 6).equals("&quot;")){
										end = n;
									}
								}
								int mod = Integer.parseInt(input.substring(start, end));
								
								lblFireAlchDamage.setForeground(Color.RED);
								lblFireAlchDamage.setText((mod) + "%");
								
								lblWaterAlchDamage.setForeground(Color.GREEN);
								lblWaterAlchDamage.setText(mod * 3 + "%");
								
								lblClayAlchDamage.setForeground(Color.RED);
								lblClayAlchDamage.setText(mod + "%");
								
								lblWindAlchDamage.setForeground(Color.RED);
								lblWindAlchDamage.setText(mod + "%");
								
								((Item) item).setAlchemyModifier(mod);
								((Item) item).setElement("water");
								
							}else if(input.contains("earth")){
								
								int start = 0, end = input.length();
								start = input.indexOf("AlchemyElementalBonus=&quot;") + 28;
								for(int n = start; n < end; n++){
									if(input.substring(n, n + 6).equals("&quot;")){
										end = n;
									}
								}
								int mod = Integer.parseInt(input.substring(start, end));
								
								lblFireAlchDamage.setForeground(Color.RED);
								lblFireAlchDamage.setText((mod) + "%");
								
								lblWaterAlchDamage.setForeground(Color.RED);
								lblWaterAlchDamage.setText(mod + "%");
								
								lblClayAlchDamage.setForeground(Color.GREEN);
								lblClayAlchDamage.setText(mod * 3 + "%");
								
								lblWindAlchDamage.setForeground(Color.RED);
								lblWindAlchDamage.setText(mod + "%");
								
								((Item) item).setAlchemyModifier(mod);
								((Item) item).setElement("earth");
								
							}
						}
						
						if(input.contains("not_ego")){
							((Item) item).setSpirit(false);
							debug.println("Found not_ego trait...", false);
						}
						if(input.contains("CanReforge=")){
							
							debug.println("Found reforge state...", false);
						}
						if(input.contains("CanEnchant=")){
							
							debug.println("Found enchant state...", false);
							
						}
						// TODO: Add new if statements for item info just above
						// this line.
					}
				}
				in.close();
				pageName = pageName.replace("Fomor_", "");
				pageName = pageName.replace("Bargain_", "");
				pageName = pageName.replace("_(2nd_generation)", "");
				pageName = pageName.replace("Beginner_", "");
				pageName = pageName.replace("_(Handicrafted)", "");
				pageName = pageName.replace("_(Blacksmithed)", "");
				URL img = new URL("http://wiki.mabinogiworld.com/view/File:" + pageName + ".png");
				in = new BufferedReader(new InputStreamReader(img.openStream()));
				input = "SOL";
				boolean run = true;
				while((input = in.readLine()) != null && run){
					if(input.contains("<img alt=\"File:")){
						int n = input.indexOf("src=\"") + 5;
						int start = n, end = -1;
						for(; n < input.length(); n++){
							if(input.substring(n, n + 1).equals("\"")){
								end = n;
								lblItemImage.setIcon(new ImageIcon(ImageIO.read(new URL("http://wiki.mabinogiworld.com" + input.substring(start, end)))));
								((Item) item).setImage((ImageIcon) lblItemImage.getIcon());
								n = input.length();
								run = false;
							}
						}
					}
				}
				fileSDs.serializeObject(item, tempName, ".itd");
			}catch (IOException e){
				debug.println("Could not load: " + e.getMessage() + "\nCopy and past this log to report an issue.", false);
				debug.setVisible(true);
			}
		}
		UpdateItemInfo();
	}
	
	public void requestFocus() {
		frmItemWindow.requestFocus();
	}
	
	public void setVisible(boolean b) {
		frmItemWindow.setVisible(b);
	}
	
	public void toFront() {
		frmItemWindow.toFront();
		frmItemWindow.repaint();
	}
	
	private void UpdateItemsList(String filter) {
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(int i = 0; i < items.size(); i++){
			String temp = (String) items.getVal(i);
			String name = items.getName(i);
			if(temp.equals(cbxCatagory.getSelectedItem()) && name.contains(filter)){
				if(name.contains("%27")){
					name = name.replaceAll("%27", "'");
				}
				if(name.contains("_")){
					name.replaceAll("_", " ");
				}
				model.addElement(name);
			}
		}
		listItems.setModel(model);
	}
	
	private void UpdateItemInfo() {
		Item i = (Item) item;
		lblName.setText(i.getName());
		if(i.isSpUp()){
			lblSpUp.setText("\u2714");
		}
		lblDamage.setText(i.getMinDamage() + " ~ " + i.getMaxDamage());
		lblBalance.setText(i.getBalance() + "%");
		lblDurability.setText(Integer.toString(i.getDurability()));
		lblInjury.setText(i.getMinInjury() + " ~ " + i.getMaxInjury());
		lblCritical.setText(i.getCritical() + "%");
		lblUpgrade.setText(i.getUpgrades() + "/" + i.getGemUpgrades());
		lblDefense.setText(Integer.toString(i.getDefense()));
		lblProtection.setText(Integer.toString(i.getProtection()));
		if(i.getElement() != null){
			int mod = i.getAlchemyModifier();
			if(i.getElement().equals("fire")){
				lblFireAlchDamage.setForeground(Color.GREEN);
				lblFireAlchDamage.setText((mod * 3) + "%");
				
				lblWaterAlchDamage.setForeground(Color.RED);
				lblWaterAlchDamage.setText(mod + "%");
				
				lblClayAlchDamage.setForeground(Color.RED);
				lblClayAlchDamage.setText(mod + "%");
				
				lblWindAlchDamage.setForeground(Color.RED);
				lblWindAlchDamage.setText(mod + "%");
			}else if(i.getElement().equals("wind")){
				lblFireAlchDamage.setForeground(Color.RED);
				lblFireAlchDamage.setText((mod) + "%");
				
				lblWaterAlchDamage.setForeground(Color.RED);
				lblWaterAlchDamage.setText(mod + "%");
				
				lblClayAlchDamage.setForeground(Color.RED);
				lblClayAlchDamage.setText(mod + "%");
				
				lblWindAlchDamage.setForeground(Color.GREEN);
				lblWindAlchDamage.setText(mod * 3 + "%");
			}else if(i.getElement().equals("water")){
				lblFireAlchDamage.setForeground(Color.RED);
				lblFireAlchDamage.setText((mod) + "%");
				
				lblWaterAlchDamage.setForeground(Color.GREEN);
				lblWaterAlchDamage.setText(mod * 3 + "%");
				
				lblClayAlchDamage.setForeground(Color.RED);
				lblClayAlchDamage.setText(mod + "%");
				
				lblWindAlchDamage.setForeground(Color.RED);
				lblWindAlchDamage.setText(mod + "%");
			}else if(i.getElement().equals("earth")){
				lblFireAlchDamage.setForeground(Color.RED);
				lblFireAlchDamage.setText((mod) + "%");
				
				lblWaterAlchDamage.setForeground(Color.RED);
				lblWaterAlchDamage.setText(mod + "%");
				
				lblClayAlchDamage.setForeground(Color.GREEN);
				lblClayAlchDamage.setText(mod * 3 + "%");
				
				lblWindAlchDamage.setForeground(Color.RED);
				lblWindAlchDamage.setText(mod + "%");
			}
		}
		lblItemImage.setIcon(i.getImage());
	}
}
