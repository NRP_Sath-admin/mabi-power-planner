package mpp;

import javax.swing.JFrame;

import mpp.util.Item;
import mpp.util.Settings;
import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.SplashScreen;
import java.awt.Toolkit;

import javax.swing.JPanel;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.Window.Type;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.border.BevelBorder;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.util.ArrayList;

import javax.swing.JSeparator;

import royalalchemi.util.FileSDs;

import java.io.File;
import java.io.IOException;

/**
 * 
 * @author Sath
 * @version 0.1.1
 */
public class Main {
	
	private final class OpenEnchantList implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			el.setVisible(true);
		}
	}
	
	private ArrayList<Item>		items;
	private JFrame				frmMabiPowerPlanner;
	private static Debug		debug;
	private static ItemWindow	itemWindow;
	private static EnchantList	el;
	private static SplashScreen	mySplash;
	private JPanel				pnlA1;
	private JPanel				pnlH;
	private JPanel				pnlA2;
	private JPanel				pnlRH;
	private JPanel				pnlBy;
	private JPanel				pnlLH;
	private JPanel				pnlG;
	private JPanel				pnlBt;
	private JPanel				pnlR;
	private JLabel				lblAccessory1;
	private JLabel				lblHelmet;
	private JLabel				lblAccessory2;
	private JLabel				lblRightHand;
	private JLabel				lblBody;
	private JLabel				lblLeftHand;
	private JLabel				lblGloves;
	private JLabel				lblBoots;
	private JLabel				lblRobe;
	private JLabel				lblAgeFinal;
	private JLabel				lbl_InjuryRate_FE;
	private JLabel				lblMagicAttack_FE;
	private JLabel				lblDamage_FE;
	private JLabel				lblLuck_FE;
	private JLabel				lblWill_FE;
	private JLabel				lblDex_FE;
	private JLabel				lblInt_FE;
	private JLabel				lblStr_FE;
	private JLabel				lblMaxStam_FE;
	private JLabel				lblMaxMp_FE;
	private JLabel				lblMaxHP_FE;
	private JLabel				lblArmorPierce_FE;
	private JLabel				lblMagicProt_FE;
	private JLabel				lblMagicDef_FE;
	private JLabel				lblProtection_FE;
	private JLabel				lblDefense_FE;
	private JLabel				lblBalance_FE;
	private JLabel				lblCritical_FE;
	private FileSDs				fileSDs;
	private Settings			settings;
	
	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		splashInit();
		EventQueue.invokeLater(new Runnable(){
			private Main	window;
			
			@Override public void run() {
				try{
					debug = new Debug();
					debug.println("Program started...", false);
					el = new EnchantList(debug, mySplash);
					window = new Main();
					window.frmMabiPowerPlanner.setVisible(true);
					itemWindow = new ItemWindow(debug, window);
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try{
			// Set System L&F
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}catch (UnsupportedLookAndFeelException e){}catch (ClassNotFoundException e){}catch (InstantiationException e){}catch (IllegalAccessException e){}
		
		settings = new Settings();
		
		String OS = System.getProperty("os.name").toUpperCase();
		String loc = "";
		
		if(OS.contains("WIN")){
			loc = System.getenv("APPDATA") + "\\MPP\\";
			
		}else if(OS.contains("MAC")){
			loc = System.getProperty("user.home") + "\\Library\\Application\\MPP\\";
			
		}else if(OS.contains("NUX")){
			loc = System.getProperty("user.dir") + "\\MPP\\";
		}
		
		fileSDs = new FileSDs(loc);
		
		if(new File(loc + "Settings.sts").exists()){
			try{
				settings = (Settings) fileSDs.deserializeObject(new File(loc + "Settings.sts"));
			}catch (ClassNotFoundException e1){
				e1.printStackTrace();
			}catch (IOException e1){
				e1.printStackTrace();
			}
		}else{
			settings.schedual();
			try{
				fileSDs.serializeObject(settings, "Settings", ".sts");
			}catch (IOException e2){
				e2.printStackTrace();
			}
		}
		
		DateFormat currentDate = DateFormat.getDateInstance();
		System.out.println("Update on " + currentDate.format(settings.getUPDATE()));
		
		items = new ArrayList<Item>();
		for(int i = 0; i < 9; i++){
			items.add(null);
		}
		
		frmMabiPowerPlanner = new JFrame();
		frmMabiPowerPlanner.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMabiPowerPlanner.setType(Type.NORMAL);
		frmMabiPowerPlanner.setBounds(100, 100, 828, 615);
		frmMabiPowerPlanner.setIconImage(Toolkit.getDefaultToolkit().getImage(Main.class.getResource("/mpp/img/mpp_icon.png")));
		frmMabiPowerPlanner.setTitle("Mabinogi Power Planner [Cache Updated: " + currentDate.format(settings.getUPDATED()) + "] Version 0.1.0");
		frmMabiPowerPlanner.getContentPane().setLayout(new MigLayout("", "[grow][]", "[grow]"));
		
		JPanel panel = new JPanel();
		frmMabiPowerPlanner.getContentPane().add(panel, "cell 0 0,grow");
		panel.setLayout(new MigLayout("", "[grow][grow][grow]", "[grow][][grow][][grow][]"));
		
		pnlA1 = new JPanel();
		pnlA1.addMouseListener(new MouseAdapter(){
			@Override public void mouseClicked(MouseEvent e) {
				if(pnlA1.getBackground().getRGB() == -986896){
					pnlA1.setBackground(Color.ORANGE);
					itemWindow.setVisible(true);
					itemWindow.requestFocus();
				}else{
					pnlA1.setBackground(new Color(-986896));
				}
				pnlH.setBackground(new Color(-986896));
				pnlA2.setBackground(new Color(-986896));
				pnlRH.setBackground(new Color(-986896));
				pnlBy.setBackground(new Color(-986896));
				pnlLH.setBackground(new Color(-986896));
				pnlG.setBackground(new Color(-986896));
				pnlBt.setBackground(new Color(-986896));
			}
		});
		pnlA1.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(pnlA1, "cell 0 0,grow");
		pnlA1.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		lblAccessory1 = new JLabel("Accessory 1");
		pnlA1.add(lblAccessory1, "cell 0 0,alignx center");
		
		pnlH = new JPanel();
		pnlH.addMouseListener(new MouseAdapter(){
			@Override public void mouseClicked(MouseEvent e) {
				if(pnlH.getBackground().getRGB() == -986896){
					pnlH.setBackground(Color.ORANGE);
					itemWindow.setVisible(true);
					itemWindow.requestFocus();
				}else{
					pnlH.setBackground(new Color(-986896));
				}
				pnlA1.setBackground(new Color(-986896));
				pnlA2.setBackground(new Color(-986896));
				pnlRH.setBackground(new Color(-986896));
				pnlBy.setBackground(new Color(-986896));
				pnlLH.setBackground(new Color(-986896));
				pnlG.setBackground(new Color(-986896));
				pnlBt.setBackground(new Color(-986896));
			}
		});
		pnlH.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(pnlH, "cell 1 0,grow");
		pnlH.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		lblHelmet = new JLabel("Helment");
		pnlH.add(lblHelmet, "cell 0 0,alignx center");
		
		pnlA2 = new JPanel();
		pnlA2.addMouseListener(new MouseAdapter(){
			@Override public void mouseClicked(MouseEvent e) {
				if(pnlA2.getBackground().getRGB() == -986896){
					pnlA2.setBackground(Color.ORANGE);
					itemWindow.setVisible(true);
					itemWindow.requestFocus();
				}else{
					pnlA2.setBackground(new Color(-986896));
				}
				pnlA1.setBackground(new Color(-986896));
				pnlH.setBackground(new Color(-986896));
				pnlRH.setBackground(new Color(-986896));
				pnlBy.setBackground(new Color(-986896));
				pnlLH.setBackground(new Color(-986896));
				pnlG.setBackground(new Color(-986896));
				pnlBt.setBackground(new Color(-986896));
			}
		});
		pnlA2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(pnlA2, "cell 2 0,grow");
		pnlA2.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		lblAccessory2 = new JLabel("Accessory 2");
		pnlA2.add(lblAccessory2, "cell 0 0,alignx center");
		
		JButton btnRemoveA1 = new JButton("Remove");
		btnRemoveA1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {}
		});
		panel.add(btnRemoveA1, "cell 0 1,alignx center");
		
		JButton btnRemoveHelm = new JButton("Remove");
		panel.add(btnRemoveHelm, "cell 1 1,alignx center");
		
		JButton btnRemoveA2 = new JButton("Remove");
		panel.add(btnRemoveA2, "cell 2 1,alignx center");
		
		pnlRH = new JPanel();
		pnlRH.addMouseListener(new MouseAdapter(){
			@Override public void mouseClicked(MouseEvent arg0) {
				if(pnlRH.getBackground().getRGB() == -986896){
					pnlRH.setBackground(Color.ORANGE);
					itemWindow.setVisible(true);
					itemWindow.requestFocus();
				}else{
					pnlRH.setBackground(new Color(-986896));
				}
				pnlA1.setBackground(new Color(-986896));
				pnlH.setBackground(new Color(-986896));
				pnlA2.setBackground(new Color(-986896));
				pnlBy.setBackground(new Color(-986896));
				pnlLH.setBackground(new Color(-986896));
				pnlG.setBackground(new Color(-986896));
				pnlBt.setBackground(new Color(-986896));
			}
		});
		pnlRH.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(pnlRH, "cell 0 2,grow");
		pnlRH.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		lblRightHand = new JLabel("Right Hand");
		pnlRH.add(lblRightHand, "cell 0 0,alignx center");
		
		pnlBy = new JPanel();
		pnlBy.addMouseListener(new MouseAdapter(){
			@Override public void mouseClicked(MouseEvent arg0) {
				if(pnlBy.getBackground().getRGB() == -986896){
					pnlBy.setBackground(Color.ORANGE);
					itemWindow.setVisible(true);
					itemWindow.requestFocus();
				}else{
					pnlBy.setBackground(new Color(-986896));
				}
				pnlA1.setBackground(new Color(-986896));
				pnlH.setBackground(new Color(-986896));
				pnlA2.setBackground(new Color(-986896));
				pnlRH.setBackground(new Color(-986896));
				pnlLH.setBackground(new Color(-986896));
				pnlG.setBackground(new Color(-986896));
				pnlBt.setBackground(new Color(-986896));
			}
		});
		pnlBy.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(pnlBy, "cell 1 2,grow");
		pnlBy.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		lblBody = new JLabel("Body");
		pnlBy.add(lblBody, "cell 0 0,alignx center");
		
		pnlLH = new JPanel();
		pnlLH.addMouseListener(new MouseAdapter(){
			@Override public void mouseClicked(MouseEvent arg0) {
				if(pnlLH.getBackground().getRGB() == -986896){
					pnlLH.setBackground(Color.ORANGE);
					itemWindow.setVisible(true);
					itemWindow.requestFocus();
				}else{
					pnlLH.setBackground(new Color(-986896));
				}
				pnlA1.setBackground(new Color(-986896));
				pnlH.setBackground(new Color(-986896));
				pnlA2.setBackground(new Color(-986896));
				pnlRH.setBackground(new Color(-986896));
				pnlBy.setBackground(new Color(-986896));
				pnlG.setBackground(new Color(-986896));
				pnlBt.setBackground(new Color(-986896));
			}
		});
		pnlLH.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(pnlLH, "cell 2 2,grow");
		pnlLH.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		lblLeftHand = new JLabel("Left Hand");
		pnlLH.add(lblLeftHand, "cell 0 0,alignx center");
		
		JButton button = new JButton("Remove");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				items.set(3, null);
				boolean remove = true;
				if(items.get(5) != null){
					for(int i = 0; i < items.get(5).getNumberOfSlotTypes(); i++){
						if(items.get(5).getSlots(i).equals("lefthand")) remove = false;
					}
				}
				if(remove){
					items.set(5, null);
				}
				updateEquipment();
			}
		});
		panel.add(button, "cell 0 3,alignx center");
		
		JButton button_2 = new JButton("Remove");
		panel.add(button_2, "cell 1 3,alignx center");
		
		JButton button_3 = new JButton("Remove");
		panel.add(button_3, "cell 2 3,alignx center");
		
		pnlG = new JPanel();
		pnlG.addMouseListener(new MouseAdapter(){
			@Override public void mouseClicked(MouseEvent arg0) {
				if(pnlG.getBackground().getRGB() == -986896){
					pnlG.setBackground(Color.ORANGE);
					itemWindow.setVisible(true);
					itemWindow.requestFocus();
				}else{
					pnlG.setBackground(new Color(-986896));
				}
				pnlA1.setBackground(new Color(-986896));
				pnlH.setBackground(new Color(-986896));
				pnlA2.setBackground(new Color(-986896));
				pnlRH.setBackground(new Color(-986896));
				pnlBy.setBackground(new Color(-986896));
				pnlLH.setBackground(new Color(-986896));
				pnlBt.setBackground(new Color(-986896));
			}
		});
		pnlG.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(pnlG, "cell 0 4,grow");
		pnlG.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		lblGloves = new JLabel("Gloves");
		pnlG.add(lblGloves, "cell 0 0,alignx center");
		
		pnlBt = new JPanel();
		pnlBt.addMouseListener(new MouseAdapter(){
			@Override public void mouseClicked(MouseEvent arg0) {
				if(pnlBt.getBackground().getRGB() == -986896){
					pnlBt.setBackground(Color.ORANGE);
					itemWindow.setVisible(true);
					itemWindow.requestFocus();
				}else{
					pnlBt.setBackground(new Color(-986896));
				}
				pnlA1.setBackground(new Color(-986896));
				pnlH.setBackground(new Color(-986896));
				pnlA2.setBackground(new Color(-986896));
				pnlRH.setBackground(new Color(-986896));
				pnlBy.setBackground(new Color(-986896));
				pnlLH.setBackground(new Color(-986896));
				pnlG.setBackground(new Color(-986896));
			}
		});
		pnlBt.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(pnlBt, "cell 1 4,grow");
		pnlBt.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		lblBoots = new JLabel("Boots");
		pnlBt.add(lblBoots, "cell 0 0,alignx center");
		
		pnlR = new JPanel();
		pnlR.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.add(pnlR, "cell 2 4,grow");
		pnlR.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		lblRobe = new JLabel("Robe");
		lblRobe.setEnabled(false);
		pnlR.add(lblRobe, "cell 0 0,alignx center");
		
		JButton button_1 = new JButton("Remove");
		panel.add(button_1, "cell 0 5,alignx center");
		
		JButton button_4 = new JButton("Remove");
		panel.add(button_4, "cell 1 5,alignx center");
		
		JButton btnRobe = new JButton("Remove");
		btnRobe.setEnabled(false);
		panel.add(btnRobe, "cell 2 5,alignx center");
		
		JPanel pnlCharacterStats = new JPanel();
		frmMabiPowerPlanner.getContentPane().add(pnlCharacterStats, "cell 1 0,grow");
		pnlCharacterStats.setLayout(new MigLayout("", "[grow,fill]", "[][grow]"));
		
		JLabel lblCharacterStats = new JLabel("Character Stats");
		lblCharacterStats.setHorizontalAlignment(SwingConstants.CENTER);
		lblCharacterStats.setFont(new Font("Tahoma", Font.BOLD, 13));
		pnlCharacterStats.add(lblCharacterStats, "cell 0 0,alignx center");
		
		JPanel panel_2 = new JPanel();
		pnlCharacterStats.add(panel_2, "cell 0 1,grow");
		panel_2.setLayout(new MigLayout("", "[100.00,grow,fill][40][19.00][40px][][]", "[][][][][][][][][][][][][][][][][][][][][]"));
		
		JLabel lblStatName = new JLabel("Stat Name");
		lblStatName.setFont(new Font("Tahoma", Font.BOLD, 11));
		panel_2.add(lblStatName, "cell 0 0");
		
		JLabel lblBasemin = new JLabel("Base(Min)");
		lblBasemin.setFont(new Font("Tahoma", Font.BOLD, 11));
		panel_2.add(lblBasemin, "cell 1 0");
		
		JLabel lblBasemax = new JLabel("Base(Max)");
		lblBasemax.setFont(new Font("Tahoma", Font.BOLD, 11));
		panel_2.add(lblBasemax, "cell 3 0");
		
		JLabel lblFromEquipment = new JLabel("From Equipment");
		lblFromEquipment.setFont(new Font("Tahoma", Font.BOLD, 11));
		panel_2.add(lblFromEquipment, "cell 4 0");
		
		JLabel lblFinal = new JLabel("Final");
		lblFinal.setFont(new Font("Tahoma", Font.BOLD, 11));
		panel_2.add(lblFinal, "cell 5 0");
		
		JLabel lblAge = new JLabel("Age");
		panel_2.add(lblAge, "cell 0 1");
		
		JSpinner spnrAge = new JSpinner();
		spnrAge.setModel(new SpinnerNumberModel(new Integer(13), new Integer(13), null, new Integer(1)));
		panel_2.add(spnrAge, "cell 1 1,growx");
		
		lblAgeFinal = new JLabel("13");
		panel_2.add(lblAgeFinal, "cell 5 1");
		
		JLabel lblCurrentLevel = new JLabel("Current Level");
		panel_2.add(lblCurrentLevel, "cell 0 2");
		
		JSpinner spnrCurrentLevel = new JSpinner();
		spnrCurrentLevel.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel_2.add(spnrCurrentLevel, "cell 1 2,growx");
		
		JLabel lblLevelFinal = new JLabel("1");
		panel_2.add(lblLevelFinal, "cell 5 2");
		
		JLabel lblMaxHp = new JLabel("Max HP");
		panel_2.add(lblMaxHp, "cell 0 3");
		
		JSpinner spnrMaxHP = new JSpinner();
		spnrMaxHP.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel_2.add(spnrMaxHP, "cell 1 3,growx");
		
		lblMaxHP_FE = new JLabel("0");
		panel_2.add(lblMaxHP_FE, "cell 4 3");
		
		JLabel lblMaxMp = new JLabel("Max MP");
		panel_2.add(lblMaxMp, "cell 0 4");
		
		JSpinner spnrMaxMP = new JSpinner();
		spnrMaxMP.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		spnrMaxHP.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel_2.add(spnrMaxMP, "cell 1 4,growx");
		
		lblMaxMp_FE = new JLabel("0");
		panel_2.add(lblMaxMp_FE, "cell 4 4");
		
		JLabel lblMaxStamina = new JLabel("Max Stamina");
		panel_2.add(lblMaxStamina, "cell 0 5");
		
		JSpinner spnrMaxStam = new JSpinner();
		spnrMaxStam.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel_2.add(spnrMaxStam, "cell 1 5,growx");
		
		lblMaxStam_FE = new JLabel("0");
		panel_2.add(lblMaxStam_FE, "cell 4 5,alignx left");
		
		JLabel lblStr = new JLabel("STR");
		panel_2.add(lblStr, "cell 0 6");
		
		JSpinner spnrStr = new JSpinner();
		spnrStr.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel_2.add(spnrStr, "cell 1 6,growx");
		
		lblStr_FE = new JLabel("0");
		panel_2.add(lblStr_FE, "cell 4 6,alignx left");
		
		JLabel lblInt = new JLabel("INT");
		panel_2.add(lblInt, "cell 0 7");
		
		JSpinner spnrInt = new JSpinner();
		spnrInt.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel_2.add(spnrInt, "cell 1 7,growx");
		
		lblInt_FE = new JLabel("0");
		panel_2.add(lblInt_FE, "cell 4 7");
		
		JLabel lblDex = new JLabel("DEX");
		panel_2.add(lblDex, "cell 0 8");
		
		JSpinner spnrDex = new JSpinner();
		spnrDex.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel_2.add(spnrDex, "cell 1 8,growx");
		
		lblDex_FE = new JLabel("0");
		panel_2.add(lblDex_FE, "cell 4 8,alignx left");
		
		JLabel lblWill = new JLabel("Will");
		panel_2.add(lblWill, "cell 0 9");
		
		JSpinner spnrWill = new JSpinner();
		spnrWill.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel_2.add(spnrWill, "cell 1 9,growx");
		
		lblWill_FE = new JLabel("0");
		panel_2.add(lblWill_FE, "cell 4 9,alignx left");
		
		JLabel lblLuck = new JLabel("Luck");
		panel_2.add(lblLuck, "cell 0 10");
		
		JSpinner spnrLuck = new JSpinner();
		spnrLuck.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		panel_2.add(spnrLuck, "cell 1 10,growx");
		
		lblLuck_FE = new JLabel("0");
		panel_2.add(lblLuck_FE, "cell 4 10");
		
		JLabel lblDamage = new JLabel("Damage");
		panel_2.add(lblDamage, "cell 0 11");
		
		JSpinner spnrMinDamage = new JSpinner();
		spnrMinDamage.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spnrMinDamage, "cell 1 11,growx");
		
		JLabel label = new JLabel("~");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(label, "cell 2 11,alignx center");
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spinner, "cell 3 11,growx");
		
		lblDamage_FE = new JLabel("0~0");
		panel_2.add(lblDamage_FE, "cell 4 11");
		
		JLabel lblMagicAttack = new JLabel("Magic Attack");
		panel_2.add(lblMagicAttack, "cell 0 12");
		
		JSpinner spnrMagicAttack = new JSpinner();
		spnrMagicAttack.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spnrMagicAttack, "cell 1 12,growx");
		
		lblMagicAttack_FE = new JLabel("0");
		panel_2.add(lblMagicAttack_FE, "cell 4 12");
		
		JLabel lblInjuryRate = new JLabel("Injury Rate");
		panel_2.add(lblInjuryRate, "cell 0 13");
		
		JSpinner spnrMinInjuryRate = new JSpinner();
		spnrMinInjuryRate.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spnrMinInjuryRate, "cell 1 13,growx");
		
		JLabel label_1 = new JLabel("~");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(label_1, "cell 2 13,alignx center");
		
		JSpinner spnrMaxInjuryRate = new JSpinner();
		spnrMaxInjuryRate.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spnrMaxInjuryRate, "cell 3 13,growx");
		
		lbl_InjuryRate_FE = new JLabel("0~0");
		panel_2.add(lbl_InjuryRate_FE, "cell 4 13");
		
		JLabel lblCritical = new JLabel("Critical");
		panel_2.add(lblCritical, "cell 0 14");
		
		JSpinner spnrCritical = new JSpinner();
		spnrCritical.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spnrCritical, "cell 1 14,growx");
		
		lblCritical_FE = new JLabel("0");
		panel_2.add(lblCritical_FE, "cell 4 14");
		
		JLabel lblBalance = new JLabel("Balance");
		panel_2.add(lblBalance, "cell 0 15");
		
		JSpinner spnrBalance = new JSpinner();
		spnrBalance.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spnrBalance, "cell 1 15,growx");
		
		lblBalance_FE = new JLabel("0");
		panel_2.add(lblBalance_FE, "cell 4 15");
		
		JLabel lblDefense = new JLabel("Defense");
		panel_2.add(lblDefense, "cell 0 16");
		
		JSpinner spnrDefense = new JSpinner();
		spnrDefense.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spnrDefense, "cell 1 16,growx");
		
		lblDefense_FE = new JLabel("0");
		panel_2.add(lblDefense_FE, "cell 4 16");
		
		JLabel lblProtection = new JLabel("Protection");
		panel_2.add(lblProtection, "cell 0 17");
		
		JSpinner spnrProtection = new JSpinner();
		spnrProtection.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spnrProtection, "cell 1 17,growx");
		
		lblProtection_FE = new JLabel("0");
		panel_2.add(lblProtection_FE, "cell 4 17");
		
		JLabel lblMagicDef = new JLabel("Magic Def.");
		panel_2.add(lblMagicDef, "cell 0 18");
		
		JSpinner spnrMagicDef = new JSpinner();
		spnrMagicDef.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spnrMagicDef, "cell 1 18,growx");
		
		lblMagicDef_FE = new JLabel("0");
		panel_2.add(lblMagicDef_FE, "cell 4 18");
		
		JLabel lblMagicProt = new JLabel("Magic Prot.");
		panel_2.add(lblMagicProt, "cell 0 19");
		
		JSpinner spnrMagicProt = new JSpinner();
		panel_2.add(spnrMagicProt, "cell 1 19,growx");
		
		lblMagicProt_FE = new JLabel("0");
		panel_2.add(lblMagicProt_FE, "cell 4 19");
		
		JLabel lblArmorPierce = new JLabel("Armor Pierce");
		panel_2.add(lblArmorPierce, "cell 0 20");
		
		JSpinner spnrArmorPierce = new JSpinner();
		spnrArmorPierce.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		panel_2.add(spnrArmorPierce, "cell 1 20,growx");
		
		lblArmorPierce_FE = new JLabel("0");
		panel_2.add(lblArmorPierce_FE, "cell 4 20");
		
		JMenuBar menuBar = new JMenuBar();
		frmMabiPowerPlanner.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmSaveCharacter = new JMenuItem("Save Character");
		mnFile.add(mntmSaveCharacter);
		
		JMenuItem mntmLoadCharacter = new JMenuItem("Load Character");
		mnFile.add(mntmLoadCharacter);
		
		JSeparator separator = new JSeparator();
		mnFile.add(separator);
		
		JMenuItem mntmClearCache = new JMenuItem("Clear Cache (Next update " + currentDate.format(settings.getUPDATE()) + ")");
		mntmClearCache.setEnabled(false);
		mntmClearCache.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showOptionDialog(frmMabiPowerPlanner, "Only do this if information seems to be out of date!\nThis may put a stress on the wiki's servers, and it will\nslow down your performance!", "Warning!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, new Object[] { "Continue", "Cancle" }, "Cancle");
			}
		});
		mntmClearCache.setFont(new Font("Segoe UI", Font.BOLD | Font.ITALIC, 12));
		mntmClearCache.setForeground(Color.RED);
		mnFile.add(mntmClearCache);
		
		JMenu mnView = new JMenu("View");
		menuBar.add(mnView);
		
		JMenuItem mntmItems = new JMenuItem("Items");
		mntmItems.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				itemWindow.setVisible(true);
			}
		});
		mnView.add(mntmItems);
		
		JMenuItem mntmEnchants = new JMenuItem("Enchants");
		mntmEnchants.addActionListener(new OpenEnchantList());
		mnView.add(mntmEnchants);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmDebug = new JMenuItem("Debug");
		mntmDebug.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				debug.setVisible(true);
			}
		});
		mnHelp.add(mntmDebug);
		
		if(settings.isUpdateDay()){
			clearCache();
		}else{
			el.loadEnchantList();
		}
	}
	
	public void clearCache(){
		int n = JOptionPane.showConfirmDialog(frmMabiPowerPlanner, "It's been 60 (or more) days since your cache was last cleared.\nDo you want to clear now?", "An Inane Question", JOptionPane.YES_NO_OPTION);
		if(n == JOptionPane.YES_OPTION){
			File items = null;
			File enchants = null;
			String OS = System.getProperty("os.name").toUpperCase();
			
			if(OS.contains("WIN")){
				items = new File(System.getenv("APPDATA") + "\\MPP\\Items\\");
				enchants = new File(System.getenv("APPDATA") + "\\MPP\\Enchant_Data\\");
				
			}else if(OS.contains("MAC")){
				items = new File(System.getProperty("user.home") + "\\Library\\Application\\MPP\\Items\\");
				enchants = new File(System.getProperty("user.home") + "\\Library\\Application\\MPP\\Enchant_Data\\");
				
			}else if(OS.contains("NUX")){
				items = new File(System.getProperty("user.dir") + "\\MPP\\Items\\");
				enchants = new File(System.getProperty("user.dir") + "\\MPP\\Enchant_Data\\");
			}
			File[] ItemFiles = items.listFiles();
			File[] EnchantFiles = enchants.listFiles();
			for(File f : ItemFiles)
				f.delete();
			for(File f : EnchantFiles)
				f.delete();
		}
	}
	
	public void setVisible(boolean b) {
		frmMabiPowerPlanner.setVisible(b);
	}
	
	private void updateEquipment() {
		if(items.get(0) != null){
			lblAccessory1.setIcon(items.get(0).getImage());
			lblAccessory1.setText("");
		}else{
			lblAccessory1.setIcon(null);
			lblAccessory1.setText("Accessory 1");
		}
		
		if(items.get(1) != null){
			lblHelmet.setIcon(items.get(1).getImage());
			lblHelmet.setText("");
		}else{
			lblHelmet.setIcon(null);
			lblHelmet.setText("Helmet");
		}
		
		if(items.get(2) != null){
			lblAccessory2.setIcon(items.get(2).getImage());
			lblAccessory2.setText("");
		}else{
			lblAccessory2.setIcon(null);
			lblAccessory2.setText("Accessory 2");
		}
		
		if(items.get(3) != null){
			lblRightHand.setIcon(items.get(3).getImage());
			lblRightHand.setText("");
		}else{
			lblRightHand.setIcon(null);
			lblRightHand.setText("Right Hand");
		}
		
		if(items.get(4) != null){
			lblBody.setIcon(items.get(4).getImage());
			lblBody.setText("");
		}else{
			lblBody.setIcon(null);
			lblBody.setText("Body");
		}
		
		if(items.get(5) != null){
			lblLeftHand.setIcon(items.get(5).getImage());
			lblLeftHand.setText("");
		}else{
			lblLeftHand.setIcon(null);
			lblLeftHand.setText("Left Hand");
		}
		
		if(items.get(6) != null){
			lblGloves.setIcon(items.get(6).getImage());
			lblGloves.setText("");
		}else{
			lblGloves.setIcon(null);
			lblGloves.setText("Gloves");
		}
		
		if(items.get(7) != null){
			lblBoots.setIcon(items.get(7).getImage());
			lblBoots.setText("");
		}else{
			lblBoots.setIcon(null);
			lblBoots.setText("Boots");
		}
	}
	
	void addItem(Object item) {
		/*
		 * pnlA1
		 * pnlH
		 * pnlA2
		 * pnlRH
		 * pnlBy
		 * pnlLH
		 * pnlG
		 * pnlBt
		 */
		if(pnlA1.getBackground() == Color.ORANGE){
			boolean canEquip = false;
			for(int i = 0; i < ((Item)item).getNumberOfSlotTypes(); i++){
				if(((Item)item).getSlots(i).equals("accessary")) canEquip = true;
			}
			if(canEquip){
				debug.println("Adding item " + item, false);
				items.set(0, (Item) item);
			}else{
				JOptionPane.showMessageDialog(frmMabiPowerPlanner, "This item can not be put in the selected slot.", "Error", JOptionPane.ERROR_MESSAGE);
			}
			
		}
		if(pnlH.getBackground() == Color.ORANGE){
			boolean canEquip = false;
			for(int i = 0; i < ((Item)item).getNumberOfSlotTypes(); i++){
				if(((Item)item).getSlots(i).equals("helmet")) canEquip = true;
			}
			if(canEquip){
				debug.println("Adding item " + item, false);
				items.set(1, (Item) item);
			}else{
				JOptionPane.showMessageDialog(frmMabiPowerPlanner, "This item can not be put in the selected slot.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		if(pnlA2.getBackground() == Color.ORANGE){
			boolean canEquip = false;
			for(int i = 0; i < ((Item)item).getNumberOfSlotTypes(); i++){
				if(((Item)item).getSlots(i).equals("accessary")) canEquip = true;
			}
			if(canEquip){
				debug.println("Adding item " + item, false);
				items.set(2, (Item) item);
			}else{
				JOptionPane.showMessageDialog(frmMabiPowerPlanner, "This item can not be put in the selected slot.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		if(pnlRH.getBackground() == Color.ORANGE){
			boolean canEquip = false;
			for(int i = 0; i < ((Item) item).getNumberOfSlotTypes(); i++){
				if(((Item) item).getSlots(i).equals("righthand")) canEquip = true;
			}
			if(canEquip){
				debug.println("Adding item " + item, false);
				items.set(3, (Item) item);
			}else{
				JOptionPane.showMessageDialog(frmMabiPowerPlanner, "This item can not be put in the selected slot.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		if(pnlBy.getBackground() == Color.ORANGE){
			boolean canEquip = false;
			for(int i = 0; i <((Item)item).getNumberOfSlotTypes(); i++){
				if(((Item)item).getSlots(i).equals("armor")) canEquip = true;
			}
			if(canEquip){
				debug.println("Adding item " + item, false);
				items.set(4, ((Item)item));
			}else{
				JOptionPane.showMessageDialog(frmMabiPowerPlanner, "This item can not be put in the selected slot.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		if(pnlLH.getBackground() == Color.ORANGE){
			
			boolean canEquip = false;
			for(int i = 0; i < ((Item) item).getNumberOfSlotTypes(); i++){
				if(((Item) item).getSlots(i).equals("lefthand")){
					canEquip = true;
				}else if(((Item) item).getSlots(i).equals("twin_sword") && items.get(3) != null){ 
					for(int n = 0; n < items.get(3).getNumberOfSlotTypes(); n++){
						if(items.get(3).getSlots(n).equals("twin_sword")) canEquip = true;
					}
				}
			}
			if(canEquip){
				debug.println("Adding item " + item, false);
				items.set(5, (Item) item);
			}else{
				JOptionPane.showMessageDialog(frmMabiPowerPlanner, "This item can not be put in the selected slot.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		if(pnlG.getBackground() == Color.ORANGE){
			boolean canEquip = false;
			for(int i = 0; i <((Item)item).getNumberOfSlotTypes(); i++){
				if(((Item)item).getSlots(i).equals("hand")) canEquip = true;
			}
			if(canEquip){
				debug.println("Adding item " + item, false);
				items.set(6, ((Item)item));
			}else{
				JOptionPane.showMessageDialog(frmMabiPowerPlanner, "This item can not be put in the selected slot.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		if(pnlBt.getBackground() == Color.ORANGE){
			boolean canEquip = false;
			for(int i = 0; i <((Item)item).getNumberOfSlotTypes(); i++){
				System.out.println(((Item) item).getSlots(i) + " =?= armor");
				if(((Item)item).getSlots(i).equals("foot")) canEquip = true;
			}
			if(canEquip){
				debug.println("Adding item " + item, false);
				items.set(7, ((Item)item));
			}else{
				JOptionPane.showMessageDialog(frmMabiPowerPlanner, "This item can not be put in the selected slot.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		updateEquipment();
		
	}
	
	public boolean isVisible() {
		return frmMabiPowerPlanner.isVisible();
	}
	
	private static void splashInit() {
		mySplash = SplashScreen.getSplashScreen();
		if(mySplash != null){ // if there are any problems displaying the splash
								// this will be null
			
			// create the Graphics environment for drawing status info
			Graphics2D splashGraphics = mySplash.createGraphics();
			Font font = new Font("Dialog", Font.PLAIN, 14);
			splashGraphics.setFont(font);
			
		}
	}
}
