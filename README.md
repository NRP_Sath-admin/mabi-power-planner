# Mabinogi Power Planner #

Current Source Version - 0.1.0 Beta

Current Stable Release Version - None

Here is a list of what is currently "working," not working, and planned. Note that the order of the planned features is not necessarily the order in which they will be added, nor is it conclusive things may be added/removed.

# Current Working Features #
* Pull and locally store data on Enchants and Items.
* Add items to a load out displaying all currently equipped gear.

# Current Broken Features / Issues #
* A few enchants do not get read properly by the program.
* Some items can't be added

# Planned Features #
* Select race and gender.
* Filter out items that can not be worn by your selected Race, Gender, or Age.
* Enchant items.
* Upgrade items.
* Gem Upgrade items.
* Special Upgrade items.
* Reforge items.
* Blacksmith items at various [grades](http://wiki.mabinogiworld.com/view/Blacksmithing#Blacksmithing_Equipment_Grades), including (for fun) S+ and X grades.
* Have stats for character be calculated where they should be (instead of inputted manually) and have equipped items affect stats.
* Save and load load outs.
* Find a way to update the item and enchants lists automatically.